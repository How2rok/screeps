var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleMiner = require('role.miner');
var roleScout = require('role.scout');
var roleTower = require('role.tower');
var respawnManager = require('respawnManager');

module.exports.loop = function ()
{
    //console.log('Tick: ' + Game.time);
    
    for(room_name in Game.rooms){
        this_room = Game.rooms[room_name];
        let room_creeps = {
            'harvester': 0,
            'upgrader': 0,
            'builder': 0,
            'miner': 0,
            'scout': 0
        };

        for(creepName in Game.creeps){
            var creep = Game.creeps[creepName];
            if(creep.room.name != room_name){
                continue;
            }
            if(creep.memory.role == 'harvester'){
                room_creeps['harvester']++;
                roleHarvester.run(creep);
            }
            if(creep.memory.role == 'upgrader'){
                room_creeps['upgrader']++;
                roleUpgrader.run(creep);
            }
            if(creep.memory.role == 'builder'){
                room_creeps['builder']++;
                roleBuilder.run(creep);
            }
            if(creep.memory.role == 'miner'){
                room_creeps['miner']++;
                roleMiner.run(creep);
            }
            if(creep.memory.role == 'scout'){
                room_creeps['scout']++;
                try{
                    roleScout.run(creep);
                }catch {

                }
            }
        }
        
        for(struct in this_room.find(FIND_STRUCTURES)){
            if(struct.structureType == STRUCTURE_TOWER && struct.my){
                roleTower.run(struct);
            }
        }
        
        respawnManager.run(this_room, room_creeps);
        //let num = respawnManager.calculate_mine_spots(this_room);
        //for(id in num){
        //    console.log(id + ": " + num[id]);
        //}
    }
}