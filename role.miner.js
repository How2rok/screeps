var roleMiner = {
    /** @param {Creep} creep**/
    run: function(creep){
        if(creep.memory.state == 'gather'){
            //console.log("miner looking")
            let source = creep.pos.findClosestByPath(FIND_SOURCES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.energy/s.energyCapacity>0.1;
                }
            });
            
            if (source == null){
                console.log("Panic!");
                return;
            }
            
            if(creep.harvest(source) == ERR_NOT_IN_RANGE){
                creep.moveTo(source, {visualizePathStyle: {stroke: '#aaaaff'}});
            }
            
            // State transition
            if(creep.store.getFreeCapacity() == 0){
                creep.memory.state = 'deliver';
            }
        }
        else if(creep.memory.state == 'deliver'){

            let target = creep.pos.findClosestByPath(FIND_STRUCTURES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.structureType==STRUCTURE_CONTAINER && s.store.getFreeCapacity(RESOURCE_ENERGY)>0;
                }
            });

            if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                creep.moveTo(target, {visualizePathStyle: {stroke: '#aaaaff'}});
            }
                           
            //State transition
            if(creep.store.getUsedCapacity() == 0){
                creep.memory.state = 'gather';
            }
        }
    }
}

module.exports = roleMiner;