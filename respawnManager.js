/**
 * @typedef {Object} RoomMemory
 * @property {boolean} simple_mining
 * @property {Int} alert_status
 * @property {Int} last_spawn_time
 */

var respawnManager =
{
    /** 
     * @param {Room} room
     * @param {Int} actualHarvester 
     * @param {Int} actualUpgrader
     * @param {Int} actualBuilder
     * @param {string:Int} room_creeps
     */
    run: function(room, room_creeps)
    {
        
        // Error checking for room memory
        if(!('alert_status' in room.memory)){
            room.memory.alert_status = 0;
        }
        if(!('simple_mining' in room.memory)){
            room.memory.simple_mining = true;
        }
        if(!('last_spawn_time' in room.memory)){
            room.memory.last_spawn_time = Game.time;
        }
        if(!('scout_target' in room.memory)){
            room.memory.scout_target = null;
        }
        // End error checking
        
        // Creep stats
        if((Game.time % 10) == 0){
            console.log("## Room: " + room.name + " Tick: " + Game.time + " Creep Status\n");
            for(creep_role in room_creeps){
                console.log(creep_role + ": " + room_creeps[creep_role]);
            }
            console.log("Simple Spawning: " + room.memory.simple_mining);
        }

        // Clear out dead creeps every 100 ticks
        if((Game.time % 100) == 0){
            for(myCreep in Memory.creeps){
                if(Game.creeps[myCreep] == undefined){
                    console.log('Found dead creep: ' + myCreep + '. Deleted from memory.');
                    delete Memory.creeps[myCreep];
                }
            }
        }

        if(room.memory.simple_mining){
            this.do_simple_mining(room, room_creeps);
        }
        else{
            this.do_advanced_spawning(room, room_creeps);
        }
         
    },


    /**
     * @param {Room} room
     * @param {string:Int} room_creeps
     */
    do_simple_mining: function(room, room_creeps){
        // TODO improve spawning
        const targetHarvester = 6;
        const targetUpgrader = 8;
        const targetBuilder = 6;

        // Keep at least 3 harvesters, then add upgraders and remaining harvesters and builders
        // for now spawn when spawner is at least 300
        if(room.energyAvailable < 300)
        {
            return;
        }
        let spawns = room.find(FIND_MY_SPAWNS);
        if(spawns.length<1){
            console.log("Can't find a spawn for Room: " + room.name);
            return;
        }

        if(room_creeps['harvester']<4){
            spawns[0].spawnCreep([WORK,WORK,CARRY,MOVE], ('Harvester' + Game.time.toString()), {memory:{role:'harvester',state:'none'}});
        }
        else if(room_creeps['upgrader']<targetUpgrader){
            spawns[0].spawnCreep([WORK,WORK,CARRY,MOVE], ('Upgrader' + Game.time.toString()), {memory:{role:'upgrader',state:'gather',idx:0}});
        }
        else if(room_creeps['harvester']<targetHarvester){
            spawns[0].spawnCreep([WORK,WORK,CARRY,MOVE], ('Harvester' + Game.time.toString()), {memory:{role:'harvester',state:'none'}});
        }
        else if(room_creeps['builder']<targetBuilder){
            spawns[0].spawnCreep([WORK,CARRY,CARRY,MOVE], ('Builder' + Game.time.toString()), {memory:{role:'builder',state:'gather',idx:0,seed: Math.random()}});
        }
        room.memory.last_spawn_time = Game.time
    },


    /**
     * @param {Room} room
     * @param {string:Int} room_creeps
     */
    do_advanced_spawning: function(room, room_creeps){
        //console.log("advanced spawning");
        if(room.memory.alert_status){
            console.log("We need to fight, spawn fighter?");
        }
        let total_creeps = 0;
        for(creep_role in room_creeps){
            total_creeps += room_creeps[creep_role];
        }
        if(total_creeps < 10){
            console.log("Possible population crash, revert to simple mining");
            room.memory.simple_mining = true;
        }

        // Fill up our energy to spawn the biggest creep
        if(room.energyAvailable<room.energyCapacityAvailable){
            return;
        }
        console.log(room.name + " ready to spawn!");

        // We have full energy in this room now
        // Here are our target spawn cap
        const total_creep_limit = 25;
        if(total_creeps>=total_creep_limit){
            //We have too many creeps, don't spawn
            return;
        }
        
        /**Here is where the spawn targets get set. we use an ordered map. When the total number of creeps is less than the target
        *  we need to spawn more. The current percentages are calculated, then any creep role less than the target percentage is spawned
        *  the order in the map is the order of spawn priority. Creep roles at the start of the list will be spawned before others
        *
        * @type {[{string:Int}]} spawn_targets
        * TODO: make this target percentage dynamic? maybe based on source utilization, active contruction projects, etc
        */
        //let spawn_targets = [];
        //spawn_targets.append({harvester: 0.25});
        //spawn_targets.append({upgrader: 0.30});
        //spawn_targets.append({builder: 0.25});
        //spawn_targets.append({miner: 0.10});

        const targetHarvester = 5;
        const targetUpgrader = 5;
        const targetBuilder = 5;
        let targetMiner = 0;
        let miner_target_id = null;
        let mining_spots = this.calculate_mine_spots(room);
        let spots_left = {};
        for(let source_id in mining_spots){
            targetMiner += mining_spots[source_id];
            spots_left[source_id] += mining_spots[source_id];
            for(creepName in Game.creeps){
                var creep = Game.creeps[creepName];   
                if(creep.memory.role == 'miner' && creep.memory.target == source_id){
                    spots_left[source_id] -= 1;
                }

            }
        }
        for(let source_id in spots_left){
            if(spots_left[source_id]>0){
                miner_target_id = source_id;
                break;
            }
        }

        let template_harvester = {
                creep_type:"harvester",
                creep_part_base: [WORK,WORK,CARRY,MOVE], 
                creep_part_list: [CARRY, MOVE],
                creep_memory: {role:'harvester',state:'gather'}};
        let template_upgrader = {
                creep_type:"upgrader",
                creep_part_base: [WORK,WORK,CARRY,MOVE], 
                creep_part_list: [CARRY, MOVE],
                creep_memory: {role:'upgrader',state:'gather'}};
        let template_builder = {
                creep_type:"builder",
                creep_part_base: [WORK,WORK,CARRY,MOVE], 
                creep_part_list: [CARRY, MOVE],
                creep_memory: {role:'builder',state:'gather',idx:0,seed:Math.random()}};
        let template_miner = {
                creep_type:"miner",
                creep_part_base: [MOVE,CARRY,WORK], 
                creep_part_list: [WORK],
                creep_memory: {role:'miner',state:'gather',target:miner_target_id,seed:Math.random()}};
        let template_scout = {
                creep_type:"scout",
                creep_part_base: [MOVE,CARRY,WORK], 
                creep_part_list: [CLAIM],
                creep_memory: {role:'scout',state:'scout',target:room.memory.scout_target,seed:Math.random()}};
        let template_scout_builder = {
                creep_type:"scout_builder",
                creep_part_base: [WORK,WORK,WORK,CARRY,MOVE], 
                creep_part_list: [CARRY, MOVE],
                creep_memory: {role:'scout',state:'scout',target:room.memory.scout_target,seed:Math.random()}};

        if(room_creeps['harvester']<4){
            this.spawn_creep_from_template(room, template_harvester, 400);
        }
        else if(room_creeps['upgrader']<targetUpgrader){
            this.spawn_creep_from_template(room, template_upgrader, 400);
        }
        else if(room_creeps['harvester']<targetHarvester){
            this.spawn_creep_from_template(room, template_harvester, 500);
        }
        else if(room_creeps['builder']<targetBuilder){
            this.spawn_creep_from_template(room, template_builder, 500);
        }
        else if(room_creeps['miner']<Math.floor(targetMiner/2)){
            this.spawn_creep_from_template(room, template_miner, 600);
        }
        //else if (room.memory.scout_target != null){
        //    console.log("Scout for " + room.memory.scout_target);
        //    this.spawn_creep_from_template(room, template_scout, 800);
        //    room.memory.scout_target = null; // Stop targetting this new room aka only spawn one scout
        //}
        else if (room.memory.scout_target != null){
            this.spawn_creep_from_template(room, template_scout_builder, 500);
            room.memory.scout_target = null; // Stop targetting this new room aka only spawn one scout
        }
        else{
            console.log("No spawn conditions met");
        }

        room.memory.last_spawn_time = Game.time
    },

    /**
     * @param {Room} room
     * @param {Object} template
     * @param {string} template.creep_type
     * @param {[string]} template.creep_part_base
     * @param {[string]} template.creep_part_list
     * @param {Object} template.creep_memory
     * @param {Int} cost_cap
     */
    spawn_creep_from_template: function(room, template, cost_cap){
        let creep_type = template.creep_type;
        let creep_part_base = template.creep_part_base;
        let creep_part_list = template.creep_part_list;
        let creep_memory = template.creep_memory;
        let spawns = room.find(FIND_MY_SPAWNS);

        if(spawns.length<1){
            console.log("Can't find a spawn for Room: " + room.name);
            return;
        }
        
        let proposed_cost = 0;

        /**@type {[string]} */
        let parts_list = [];
        for(let part of creep_part_base){
            let this_part_cost = BODYPART_COST[part];
            if(proposed_cost + this_part_cost > Math.min(room.energyAvailable, cost_cap)){
                console.log("Creep base could not be satisfied\n"+creep_part_base);
                break;
            }
            parts_list.push(part);
            proposed_cost += this_part_cost;
        }
        while(parts_list.length<50){
            let proposed_part = creep_part_list[parts_list.length % creep_part_list.length];
            let this_part_cost = BODYPART_COST[proposed_part];
            if(proposed_cost + this_part_cost > Math.min(room.energyAvailable, cost_cap)){
                break;
            }
            parts_list.push(proposed_part);
            proposed_cost += this_part_cost;
        }
                
        spawns[0].spawnCreep(parts_list,
                creep_type + "_" + Game.time.toString(),
                {memory: creep_memory});

    },

    /**
     * @param {Room} room 
     * @returns {string:Int} num_spots
     */
    calculate_mine_spots: function(room){

        /**@type {string:Int} */
        let num_spots = {};

        /**@type {Terrain} */
        let terrain = room.getTerrain();
        for(let source of room.find(FIND_SOURCES)){
            num_spots[source.id] = 0;
            for(let x = Math.max(0,source.pos.x-1); x<=Math.min(49,source.pos.x+1); x++){
                for(let y = Math.max(0,source.pos.y-1); y<=Math.min(49,source.pos.y+1); y++){
                    //console.log("X: " + x + " Y: " + y + " = " + terrain.get(x,y));
                    if(!terrain.get(x,y)){
                        num_spots[source.id]++;
                    }
                }                
            }
        }

        //console.log("There are " + num_spots + " minging spots in " + room.name);
        return num_spots;
    }
    
}
module.exports = respawnManager;