var roleHarvester = {

    /** @param {Creep} creep**/
    run: function(creep){
        if(creep.memory.state == "none"){
            creep.memory.state = "gather";
        }
        
        if(creep.memory.state == "gather"){
            let container = creep.pos.findClosestByPath(FIND_STRUCTURES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.structureType==STRUCTURE_CONTAINER && 
                    s.store.getUsedCapacity(RESOURCE_ENERGY)>creep.store.getFreeCapacity(RESOURCE_ENERGY);
                }
            });
            let source = creep.pos.findClosestByPath(FIND_SOURCES,
            {
                filter: function(s){
                    return s.room.name == creep.room.name && s.energy/s.energyCapacity>0.1;
                }
            });
            
            if (container == null && source == null){
                console.log("Panic!");
                return;
            }
            
            if(container != null){
                if(creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                    creep.moveTo(container, {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
            else if(creep.harvest(source) == ERR_NOT_IN_RANGE){
                creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
            }

            // State transition
            if(creep.store.getFreeCapacity() == 0){
                creep.memory.state = 'deliver';
            }
        }
        else if(creep.memory.state == 'deliver'){
            let spawner = creep.pos.findClosestByPath(FIND_STRUCTURES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.structureType==STRUCTURE_SPAWN && s.store.getFreeCapacity(RESOURCE_ENERGY)>0;
                }
            });
            if(spawner != null && spawner.store.getFreeCapacity(RESOURCE_ENERGY)>0 && creep.transfer(spawner, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                creep.moveTo(spawner, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            else{
                for(let target of creep.room.find(FIND_MY_STRUCTURES)){
                    if(target.structureType == STRUCTURE_EXTENSION && target.store.getFreeCapacity(RESOURCE_ENERGY)>0){
                        if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                            creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                        }
                        break;
                    }
                }
            }

            //State transition
            if(creep.store.getUsedCapacity() == 0){
                creep.memory.state = 'gather';
            }
        }
    }
};


module.exports = roleHarvester;