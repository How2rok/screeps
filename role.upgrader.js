var roleUpgrader = {

    /** @param {Creep} creep**/
    run: function(creep){
        if(creep.memory.state == 'gather'){
            let container = creep.pos.findClosestByPath(FIND_STRUCTURES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.structureType==STRUCTURE_CONTAINER &&
                    s.store.getUsedCapacity(RESOURCE_ENERGY)>creep.store.getFreeCapacity(RESOURCE_ENERGY);
                }
            });
            let source = creep.pos.findClosestByPath(FIND_SOURCES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.energy/s.energyCapacity>0.1;
                }
            });
            
            if (container == null && source == null){
                console.log("Panic!");
                return;
            }


            if(container != null){
                if(creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                    creep.moveTo(container, {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
            else if(creep.harvest(source) == ERR_NOT_IN_RANGE){
                creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
            }
            
            // State transition
            if(creep.store.getFreeCapacity() == 0){
                creep.memory.state = 'deliver';
            }
        }
        else if(creep.memory.state == 'deliver'){
            var controller = creep.room.controller;
            if(creep.transfer(controller, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                creep.moveTo(controller, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            
            //State transition
            if(creep.store.getUsedCapacity() == 0){
                creep.memory.state = 'gather';
                var sources = creep.room.find(FIND_SOURCES);
                var idx = Math.floor(Math.random() * sources.length); //TODO: Bias towards source 1
                creep.memory.idx = idx;
            }
        }
    }
};


module.exports = roleUpgrader;