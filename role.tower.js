var roleTower = 
{
    /** @param {StructureTower} tower**/
    run: function(tower)
    {
        //console.log("I am a tower with ID: " + tower.id);

        let canFire = false;
        let target = null;

        if(tower.store.getUsedCapacity(RESOURCE_ENERGY)>10){canFire = true;}

        if(canFire){
            let target_dist_sqr = Infinity;
            for( potential_target of tower.room.find(FIND_HOSTILE_CREEPS)){
                if(target==null){
                    target = potential_target;
                    continue;
                }
                potential_target_distance_sqr = (tower.pos.x-potential_target.pos.x)**2 + (tower.pos.y-potential_target.pos.y)**2;
                if(target_dist_sqr>potential_target_distance_sqr){
                    target = potential_target;
                    target_dist_sqr = potential_target_distance_sqr;
                }

            }

            if(target!=null){
                tower.attack(target);
            }
        }

        




    }
};


module.exports = roleTower;