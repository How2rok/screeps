var roleScout = {
    /** @param {Creep} creep**/
    run: function(creep){
        if(creep.memory.state == 'scout'){
            // Are we in the right room?
            if(creep.room.name != creep.memory.target){
                // Move to that room
                // we don't know where the controller is so go to middle
                creep.moveTo(new RoomPosition(25, 25, creep.memory.target));
            }
            else {
                // We are in the target room. Try to claim controller if it's not ours
                if(!creep.room.controller.my){
                    let ret = creep.claimController(creep.room.controller);
                    if(ret == ERR_NOT_IN_RANGE){
                        creep.moveTo(creep.room.controller);
                    }
                    else{
                        console.log("Claim controller failed with: " + ret);
                    }
                }
                else {
                    //fix bug where creep re-enters previous room
                    creep.moveTo(creep.room.controller);
                    // This controller is ours already convert to a builder to build a spawn
                    creep.memory.state = "gather";
                    creep.memory.role = "builder";
                }
            }
            
        }
        else if(creep.memory.state == 'deliver'){

                                      
            //State transition
            if(creep.store.getUsedCapacity() == 0){
                creep.memory.state = 'scout';
            }
        }
    }
}

module.exports = roleScout;