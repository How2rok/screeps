var roleBuilder = {

    /** @param {Creep} creep**/
    run: function(creep){
        if(creep.memory.state == 'gather'){
            let container = creep.pos.findClosestByPath(FIND_STRUCTURES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.structureType==STRUCTURE_CONTAINER &&
                    s.store.getUsedCapacity(RESOURCE_ENERGY)>creep.store.getFreeCapacity(RESOURCE_ENERGY);
                }
            });
            let source = creep.pos.findClosestByPath(FIND_SOURCES,{
                filter: function(s){
                    return s.room.name == creep.room.name && s.energy/s.energyCapacity>0.1;
                }
            });
            
            if (container == null && source == null){
                console.log("Panic!");
                return;
            }
                

            if(container != null){
                if(creep.withdraw(container, RESOURCE_ENERGY)== ERR_NOT_IN_RANGE){
                    creep.moveTo(container, {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
            else if(creep.harvest(source) == ERR_NOT_IN_RANGE){
                creep.moveTo(source, {visualizePathStyle: {stroke: '#ffaa00'}});
            }
            
            // State transition
            if(creep.store.getFreeCapacity() == 0){
                creep.memory.state = 'deliver';
            }
        }
        else if(creep.memory.state == 'deliver'){
            // Build stuff first
            let hasTasks = false;
            let targets = creep.room.find(FIND_MY_CONSTRUCTION_SITES);
            if(targets.length>0 && creep.memory.seed<0.60){
                hasTasks = true;
                if(creep.build(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
            else{
                // Find things that need repair
                if(!hasTasks){
                    // First build up ramparts to 500
                    for(let struct of creep.room.find(FIND_STRUCTURES,{
                        filter: function(struct){
                        return struct.structureType == STRUCTURE_RAMPART && struct.my && struct.hits<500;}}))
                    {
                        console.log("repair ramparts?");
                        hasTasks = true;
                        this.repair_or_move_to(creep, struct);break;
                    }
                }

                if(!hasTasks){
                    // now repair up towers to 100%
                    for(let struct of creep.room.find(FIND_STRUCTURES,{
                        filter: function(struct){
                            return struct.structureType == STRUCTURE_TOWER && struct.hits<struct.hitsMax;}}))
                    {
                        hasTasks = true;
                        this.repair_or_move_to(creep, struct);break;
                    }
                }

                if(!hasTasks){
                    // now fill up towers to 100% energy
                    for(let struct of creep.room.find(FIND_STRUCTURES,{
                        filter: function(struct){
                            return struct.structureType == STRUCTURE_TOWER && 
                            struct.store.getUsedCapacity(RESOURCE_ENERGY)<struct.store.getCapacity(RESOURCE_ENERGY);}}))
                    {
                        hasTasks = true;
                        this.transfer_or_move_to(creep, struct);break;
                    }
                }
                if(!hasTasks){
                    // Then build up walls to 500
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType == STRUCTURE_WALL && struct.hits<500){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }
                if(!hasTasks){
                    // Then build up roads to 80%
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType == STRUCTURE_ROAD &&
                            struct.hits/struct.hitsMax<0.8){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }
                if(!hasTasks){
                    // Then back to building up ramparts to 5000
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType == STRUCTURE_RAMPART &&
                            struct.my && struct.hits<5000){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }

                if(!hasTasks){
                    // Then back to building up walls to 5000
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType == STRUCTURE_WALL &&
                            struct.hits<10000){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }

                if(!hasTasks){
                    // Now repair stuff that is not walls or ramparts to 80%
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType != STRUCTURE_WALL && struct.structureType != STRUCTURE_RAMPART && struct.hits/struct.hitsMax<0.8){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }

                if(!hasTasks){
                            // Then back to building up walls to 25000
                    for(let struct of creep.room.find(FIND_STRUCTURES)){
                        if(struct.structureType == STRUCTURE_WALL &&
                            struct.hits<25000){
                            hasTasks = true;
                            this.repair_or_move_to(creep, struct);break;
                        }
                    }
                }

                
                if(!hasTasks){
                    // now fill up ramparts to 25000
                    for(let struct of creep.room.find(FIND_STRUCTURES, {
                        filter: function(struct){
                            return struct.structureType == STRUCTURE_RAMPART && struct.hits<25000;}})){
                                hasTasks = true;
                                this.repair_or_move_to(creep, struct);break;
                    }
                }
                
                if(!hasTasks){
                    // If there is really nothing left to do, go back to building
                    if(targets.length>0){
                        hasTasks = true;
                        if(creep.build(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                            creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
                        }
                    }
                }
            }
            if (!hasTasks){
                // not much else to do except stock up for a furtue task
                if(creep.store.getFreeCapacity(RESOURCE_ENERGY) > 10){
                    // transition to deliver
                    hasTasks = true;
                    creep.memory.state = 'gather';
                }
            }

            if (!hasTasks){
                // Nothing left to do move to parking
                for(flag of creep.room.find(FIND_FLAGS)){
                    if (flag.name == "builder_parking"){
                        creep.moveTo(flag, {visualizePathStyle: {stroke: '#ff0000'}});
                    }
                }
            }
            //State transition
            if(creep.store.getUsedCapacity() == 0){
                creep.memory.state = 'gather';
                var sources = creep.room.find(FIND_SOURCES);
                var idx = Math.floor(Math.random() * sources.length);
                creep.memory.idx = idx;
            }
        }
    },

    /**
     *  @param {Creep} creep
     *  @param {Struture} target
    */
    repair_or_move_to: function(creep, target){
        let ret = creep.repair(target)
        if(ret == ERR_NOT_IN_RANGE){
            creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
        }else if(ret!=0){
            console.log(creep.name + " got error "+ ret + " while repairing " + target.id);
        }
    },

    /**
     *  @param {Creep} creep
     *  @param {Struture} target
     */
    transfer_or_move_to: function(creep, target){
        let ret = creep.transfer(target, RESOURCE_ENERGY)
        if(ret == ERR_NOT_IN_RANGE){
            creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
        }else if(ret!=0){
            console.log(creep.name + " got error "+ ret + " while transfering " + target.id + " at " + target.pos);
        }
    }
};


module.exports = roleBuilder;